#pragma once

#include "vector_dense_algebra.h"
#include "vector_dense_aux.h"
#include "vector_dence_constructors.h"
#include "vector_dense_declaration.h"
#include "vector_dense_get.h"
#include "vector_dense_memory.h"
#include "vector_dense_operators.h"
#include "vector_dense_set.h"
#include "vector_dense_stl_members.h"
