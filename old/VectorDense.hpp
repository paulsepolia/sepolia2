
#ifndef VECTOR_DENSE_H
#define VECTOR_DENSE_H

#include "VectorDenseDeclaration.hpp"

#include "VectorDenseAlgebraBasic.hpp"
#include "VectorDenseAuxiliary.hpp"
#include "VectorDenseBLAS.hpp"
#include "VectorDenseCMATH.hpp"
#include "VectorDenseComplex.hpp"
#include "VectorDenseConstructors.hpp"
#include "VectorDenseDeclaration.hpp"
#include "VectorDenseDestructor.hpp"
#include "VectorDenseGet.hpp"
#include "VectorDenseGSL.hpp"
#include "VectorDenseMemory.hpp"
#include "VectorDenseOperators.hpp"
#include "VectorDenseSet.hpp"
#include "VectorDenseSTL.hpp"

#include "VectorDenseGeneric.hpp" // after BLAS and GSL

#endif // VECTOR_DENSE_H
