#pragma once

#include <string>

namespace skz {

    const std::string E0001("ERROR: RAM is not allocated for the container");
    const std::string E0002("ERROR: Index is out of range");
    const std::string E0003("ERROR: Vectors are not of same size");

}