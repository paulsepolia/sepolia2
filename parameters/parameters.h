#pragma once

#include <cstdint>

namespace skz {

    const bool DEBUG(true);
    const uint32_t NT1D(2);

}